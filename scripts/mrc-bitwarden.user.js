// ==UserScript==
// @name         MRC Bitwarden fix
// @namespace    https://goble.dev/
// @version      1.0.0
// @description  Prevent login fields from blanking when clicked.
// @author       Jonathan Goble (jcgoble3)
// @match        https://www.myracingcareer.com/*/
// @icon         https://www.google.com/s2/favicons?domain=myracingcareer.com
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    document.querySelectorAll(".log_input").forEach((node) => node.removeAttribute("onclick"))
})();
